// Activity:
// 1. Update and Debug the following codes to ES6
//     Use template literals
//     Use array/oject destructuring
//     Use arrow function

// let student1 = {
// 	name: "Shawn Michaels",
// 	birthday: "May 5, 2003",
// 	age: 18,
// 	isEnrolled: true,
// 	classes: ["Philosphy 101", "Social Sciences 201"]
// }

// let student2 = {
// 	name "Steve Austin",
// 	birthday: "June 15, 2001",
// 	age: 20
// 	isEnrolled: true
// 	classes: ["Philosphy 401", "Natural Sciences 402",
// }

// function introduce(student){

// 	//Note: You can destructure objects inside functions.

// 	console.log("Hi! " + "I'm " + student.name + " ." + " I am " + student.ages + " years old.");
// 	console.log("I study the following courses + " classes);
// }

// function getCube(num){

// 	console.log(Math.pow(num,3));

// }

// let cube = getCube(3);

// console.log(cube)

// let numArr = [15,16,32,21,21,2]

// numArr.forEach(function(num){

// 	console.log(num);
// })

// let numsSquared = numArr.map(function(num){

// 	return num ** 2;

// })

// console.log(numSquared);

// =============================================USING TEMPLATE LITERALS=========================================================================================

let studentName1 = "Shawn Michaels";
let studentBirthday1 = "May 5, 2003";
let studentAges1 = 18;
let studentEnrolled1 = true;
let studentClasses1 = ["Philosphy 101", "Social Sciences 201"];

let sentence4 = `Hi I'm ${studentName1}. I am ${studentAges1} years old.`
let sentence5 =  `I study the following courses : ${studentClasses1}.`;
console.log(sentence4);
console.log(sentence5);



let studentName2 = "Steve Austin";
let studentBirthday2 = "June 15,2001";
let studentAges2 = 20;
let studentEnrolled2 = true;
let studentClasses2 = ["Philosphy 401", "Natural Sciences 402"];

let sentence6 = `Hi I'm ${studentName2}. I am ${studentAges2} years old.`
let sentence7 =  `I study the following courses : ${studentClasses2}.`;
console.log(sentence6);
console.log(sentence7);

// ================================================================================================================================================================

 // =============================================ARRAY DESTRUCTURING==================================================================================================


let student1 = ['Shawn Michaels','May 5, 2003', 18, true, ["Philosphy 101", "Social Sciences 201"]];
let [name, birthday, age, isEnrolled, classSubjects] = student1; 
let sentence = `Hi I'm ${name}. I am ${age} years old.`
let sentence1 = `I study the following courses: ${classSubjects}.`;
console.log(sentence);
console.log(sentence1);

let student2 = ['Steve Austin','June 15,2001', 20, true, ["Philosphy 401", "Natural Sciences 402"]];
let [name2, birthday2, age2, isEnrolled2, classSubjects2] = student2; 
let sentence2 = `Hi I'm ${name2}. I am ${age2} years old.`
let sentence3 = `I study the following courses: ${classSubjects2}.`;
console.log(sentence2);
console.log(sentence3);


// =====================================================================================================================================================================

 // =============================================ARROW FUNCTION=========================================================================================================

let studentA = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

const introduceStudentA = (studentA) => {
	console.log(`Hi I'm ${studentA.name}. I am ${studentA.age} years old.`)
    console.log (`I study the following courses : ${studentA.classes}.`);
}
    introduceStudentA(studentA);



let studentB = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}

const introduceStudentB = (studentB) => {
	console.log(`Hi I'm ${studentB.name}. I am ${studentB.age} years old.`)
    console.log (`I study the following courses : ${studentB.classes}.`);
}
    introduceStudentA(studentB);

// ======================================================================================================================================================================

// =============================================UPDATED AND DEBUGGED CODES TO ES6=========================================================================================


const getCube = (num) =>Math.pow(num,3);
let cube = getCube(3);
console.log(cube)



let numArr = [15,16,32,21,21,2];
numArr.forEach((num) => {
console.log(num);
})



let numSquared = numArr.map((num) => Math.pow(num,2));
console.log(numSquared);


// =======================================================================================================================================================================

// =============================================NO.2======================================================================================================================

// 2. Create a class constructor able to receive 3 arguments
//          -it should be able to receive two strings and a number
//          -Using the this keyword assign properties:
//          name,
//          breed,
//          dogAge = <7 * human years>
//                 - assign the parameters as values to each property.log



class dog{
	constructor (name,breed,dogAge){
		this.name = name;
		this.breed = breed;
		this.dogAge = dogAge*7;
	}
};

let dog1 = new dog('Okani','Duschand','6');
console.log(dog1);


// =======================================================================================================================================================================


// =============================================NO.3======================================================================================================================


// 3. Create 2 new objects using our class constructor
//     This constructor should be able to create Dog objects.logLog the 2 new Dog objects in the console or alert.   

let dog2 = new dog('Hachi','Siberian Husky','5');
console.log(dog2);
let dog3 = new dog('Shimaru','Rottweiler','4');
console.log(dog3);

// =======================================================================================================================================================================







